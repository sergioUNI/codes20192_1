package com.sigmotoa.codes.workshop;

import java.util.Arrays;

/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */
public class Agility {

    //bigger than
    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB)
    {
        if(Double.parseDouble(numA) > Double.parseDouble(numB) ){
            System.out.println(numA);
            return true;
        }else{
            System.out.println(numB);
            return false;
        }
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC,int numD,int numE)
    {
        int[] array = {numA,numB,numC,numD,numE};
        Arrays.sort(array);
        Arrays.toString(array);
        System.out.println(Arrays.toString(array));
        return array;
    }

    //Look for the smaller number of the array
    public static double smallerThan(double array [])
    {
        Arrays.sort(array);
        System.out.println(array[0]);
        return array[0];
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA)
    { return false; }

    //the word is palindrome
    public static boolean palindromeWord(String word)
    {
        String reversedWord = "";
        for(int i=word.length()-1; i >=0; i--){
            reversedWord = reversedWord + word.charAt(i);
        }

        if(word.equals(reversedWord)){
            return true;
        } else{
            return false;
        }

    }

    //Show the factorial number for the parameter
    public static int factorial(int numA)
    {
        int i,fact=1;
        for(i=1;i<numA;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+numA+" is: "+fact);
        return fact;
    }

    //is the number odd
    public static boolean isOdd(byte numA)
    {
        if (numA%2!=0)
            return true;
        else
            return false;
    }

    //is the number prime
    public static boolean isPrimeNumber(int numA)
    {
        int contador = 2;
        boolean primo = true;
        if(numA < 0){
            return false;
        }else{
            while ((primo) && (contador != numA)){
                if (numA % contador == 0) {
                    primo = false;
                }
                contador++;
            }
            return primo;
        }
    }

    //is the number even
    public static boolean isEven(byte numA)
    {
        if (numA%2==0){
            return true;
        } else {
            return false;
        }
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA)
    { return false; }

    //Return an array with the fibonacci sequence for the requested number
    public static int [] fibonacci(int numA)
    {
        /*int fibo1=1;
        int fibo2=1;
        int i;

        for(i=2;i<=numA;i++){
            fibo2 = fibo1 + fibo2;
            fibo1 = fibo2 - fibo1;
        }*/
        return new int[0];
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA)
    {return -1;}

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)
    /**
     * If number is divided by 3, show fizz
     * If number is divided by 5, show buzz
     * If number is divided by 3 and 5, show fizzbuzz
     * in other cases, show the number
     */
    { return null;}
}
